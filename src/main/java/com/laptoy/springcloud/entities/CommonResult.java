package com.laptoy.springcloud.entities;

import javax.persistence.Entity;

@Entity
public class CommonResult<T> {
    //响应码 如200，404 ...
    private Integer code;
    private String message;
    private T data;

    public CommonResult(Integer code, String message){
        this(code, message, null);
    }

    public CommonResult(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
